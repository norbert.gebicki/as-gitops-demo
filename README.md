# GitOps-AS-demo

## Deploying infrastructure into Azure

```
# export terraform variables with Azure credentials

export ARM_CLIENT_SECRET=<AZURE CLIENT SECRET>
export ARM_CLIENT_ID=<AZURE CLIENT ID>
export ARM_SUBSCRIPTION_ID=<AZURE SUBSCRIPTION ID>
export ARM_TENANT_ID=<AZURE TENANT ID>

terraform plan
terraform apply
```

## Deploying Flux
### on GitHub
```
export GITHUB_TOKEN=<YOUR GITHUB TOKEN>

flux bootstrap github \
  --owner=YOUR_USERNAME \
  --repository=YOUR_REPO_NAME \
  --personal
```
example:
```
flux bootstrap github \
  --owner=wotd \
  --repository=GitOps-AS-demo \
  --personal
```
### on GitLab:
```
export GITLAB_TOKEN=<YOUR GITLAB TOKEN>

flux bootstrap gitlab \
  --ssh-hostname=gitlab.com \
  --owner=norbert.gebicki \
  --repository=as-gitops-demo \
  --branch=main
```