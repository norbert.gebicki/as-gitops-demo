provider "azurerm" {
  features {}
}

data "azurerm_kubernetes_cluster" "main" {
  depends_on          = [module.aks-config] # refresh cluster state before reading
  name                = "${var.resource_prefix}-${var.environment_slug}-aks"
  resource_group_name = azurerm_resource_group.main.name
}

provider "kubernetes" {
  host                   = data.azurerm_kubernetes_cluster.main.kube_config.0.host
  client_certificate     = base64decode(data.azurerm_kubernetes_cluster.main.kube_config.0.client_certificate)
  client_key             = base64decode(data.azurerm_kubernetes_cluster.main.kube_config.0.client_key)
  cluster_ca_certificate = base64decode(data.azurerm_kubernetes_cluster.main.kube_config.0.cluster_ca_certificate)
}

resource "azurerm_resource_group" "main" {
  name     = "${var.resource_prefix}-${var.environment_slug}"
  location = var.azure_region
  tags = {
    Project = var.project
    Environment = var.environment
    Env = var.environment_slug
  }
}

resource "azurerm_public_ip" "ingress" {
  name                = "${var.resource_prefix}-${var.environment_slug}-ingress-ip"
  resource_group_name = "${var.resource_prefix}-${var.environment_slug}-aks"
  location            = azurerm_resource_group.main.location
  allocation_method   = "Static"
  sku                 = "Standard" 
  depends_on                = [module.aks-config]
  tags = {
    Project = var.project
    Environment = var.environment
    Env = var.environment_slug
  }
}

module "aks-config" {
  source              = "./aks-config"
  project             = var.project
  resource_prefix     = var.resource_prefix
  environment         = var.environment
  environment_slug    = var.environment_slug
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location
}

