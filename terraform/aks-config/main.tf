resource "azurerm_kubernetes_cluster" "main" {
  name                = "${var.resource_prefix}-${var.environment_slug}-aks"
  location            = var.location
  resource_group_name = var.resource_group_name
  dns_prefix          = "${var.resource_prefix}-${var.environment_slug}-aks"
  kubernetes_version  = "1.22.4"
  node_resource_group = "${var.resource_prefix}-${var.environment_slug}-aks"

  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    =  var.default_vm_size
  }

  addon_profile {
    aci_connector_linux {
      enabled = false
    }
    azure_policy {
      enabled = false
    }
    http_application_routing {
      enabled = false
    }
    kube_dashboard {
      enabled = false
    }
    oms_agent {
      enabled = false
    }
  }

  identity {
    type = "SystemAssigned"
  }
  
  network_profile {
    network_plugin = "kubenet"
    load_balancer_sku = "standard"
  }

  tags = {
    Project = var.project
    Environment = var.environment
    Env = var.environment_slug
  }
}