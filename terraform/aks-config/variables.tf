// Organisational Variables

variable "project" {
  type    = string
}

variable "resource_prefix" {
    type    = string
}

variable "environment" {
    type    = string
}

variable "environment_slug" {
    type    = string
}

// AKS Deployment Variables
variable "resource_group_name" {
    type    = string
}

variable "location" {
    type    = string
}

variable "default_vm_size" {
    type    = string
    default = "Standard_D4s_v3"
}