variable "azure_region" {
  type    = string
  default = "West Europe"
}

variable "project" {
  type    = string
  default = "Fujitsu Azure Summit"
}

variable "resource_prefix" {
  type    = string
  default = "as"
}

variable "environment" {
  type    = string
  default = "development"
}

variable "environment_slug" {
  type    = string
  default = "dev"
}

variable "apps_namespace" {
  type = string
  default = "apps"
}